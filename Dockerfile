FROM python:3.12-slim

WORKDIR /

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    wget \
    gnupg \
    unzip && \
    pip install --no-cache-dir \
    fastapi \
    beautifulsoup4 \
    blinker==1.7.0 \
    selenium-wire \
    uvicorn[standard] \
    webdriver_manager \
    lxml \
    python-dotenv \
    redis \
    setuptools \
    apscheduler && \
    wget -q -O - https://dl.google.com/linux/linux_signing_key.pub | apt-key add - && \
    sh -c 'echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list' && \
    apt-get update && \
    apt-get install -y google-chrome-stable && \
    wget https://storage.googleapis.com/chrome-for-testing-public/125.0.6422.141/linux64/chromedriver-linux64.zip && \
    unzip chromedriver-linux64.zip && \
    mv chromedriver-linux64 /usr/local/bin/ && \
    rm chromedriver-linux64.zip && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

COPY . .

ENV PYTHONUNBUFFERED=1
ENV PATH="/usr/local/bin:${PATH}"

EXPOSE 8001

CMD ["python", "main.py"]