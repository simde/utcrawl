# UTCrawl
## Project Description
UTCrawl is a web scraping API built with FastAPI in Python. It allows users to crawl and extract data from the Université de Technologie de Compiègne (UTC) catalog, including information about diplomas and UVs (Unités de Valeur).
## Installation and Running
1. Clone the repository:
   ```bash
   git clone git@gitlab.utc.fr:simde/utcrawl.git
   ```
2. Navigate to the project directory:
   ```bash
   cd utcrawl
   ```
3. Install Python if you don't have it yet and all its dependencies:
   ```bash
   pip install fastapi beautifulsoup4 selenium-wire uvicorn[standard] webdriver_manager lxml python-dotenv redis apscheduler
   ```
4. You must have Google Chrome installed on your computer. 
You must also have ChromeDriver corresponding to your version of Chrome. Download it and place the downloaded file in a directory accessible by your PATH.
5. Run the main Python file
   ```bash
   python main.py
   ```
## Features
### Endpoints
* `/diplomas`: Returns the list of diplomas available in the university catalog.
   * Response Structure: The response is a JSON object where each key represents a diploma code, and the corresponding value is a dictionary containing program codes and their corresponding program labels.
   * Example Structure:
      ```json
      {
         "Diploma_Code_1": {
            "Program_Code_1": "Program_Label_1",
            "Program_Code_2": "Program_Label_2"
         },
         "Diploma_Code_2": {
            "Program_Code_3": "Program_Label_3"
         }
      }
      ```

* `/uvs`: Returns the list of UVs available in the university catalog.
   * Response Structure: The response is a JSON object where each key represents a UV course code. Each UV course code is associated with a dictionary containing information such as the course title, type, semesters offered, and the programs associated with the course.
   * Example Structure:
      ```json
      {
         "UV_Code_1": {
            "Titre": "UV_Title_1",
            "Type": "UV_Type_1",
            "Semestres": [
               "Semester_1",
               "Semester_2"
            ],
            "Spécialités": [
               "Speciality_1", 
               "Speciality_2"
            ]
         },
         "UV_Code_2": {
            "Titre": "UV_Title_2",
            "Type": "UV_Type_2",
            "Semestres": [
               "Semester_1"
            ],
            "Spécialités": [
               "Speciality_3"
            ]
         }
      }
      ```
### Documentation
Access the interactive API documentation at the `/docs` endpoint after starting the server.
### Configuration
Environment Variables: make sure you have a .env file containing the API keys needed for the project with the following structure.
```json
API_KEYS={"APP_1": "APP-1-KEY", "APP_2": "APP-2-KEY"}
```
It is possible to add, modify or delete API keys in the .env file while the API is running. However, you will need to kill and then restart the Python app for the changes to take effect, or remove and then recreate if it is a Compose Docker Stack.
### Example Usage
* To retrieve the list of diplomas:
   ```bash
   curl -H "x-api-key: your-key" http://localhost:8000/diplomas
   ```
* To retrieve the list of UVs:
   ```bash
   curl -H "x-api-key: your-key" http://localhost:8000/uvs
   ```
## Credits
UTCrawl is developed by Quentin BOYER.
## License
UTCrawl is licensed under the [Creative Commons Attribution-NonCommercial 4.0 International License](https://creativecommons.org/licenses/by-nc/4.0/). This means you are free to share and adapt the code for non-commercial purposes, provided you give appropriate credit to the original author (Quentin BOYER) and indicate if changes were made. However, you may not use the code for commercial purposes without explicit permission from the author.