# Importing necessary libraries
from fastapi import FastAPI, Response, HTTPException, Depends
from fastapi.security.api_key import APIKeyHeader
import time
import json
import re
from bs4 import BeautifulSoup
from seleniumwire import webdriver
from seleniumwire.utils import decode
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from dotenv import load_dotenv
import os
import redis
from apscheduler.schedulers.background import BackgroundScheduler
from datetime import datetime, timedelta
import uvicorn


# Load environment variables
load_dotenv()

# Initialize API key header for FastAPI
api_key_header = APIKeyHeader(name="x-api-key")

# Load API keys from environment variable
api_keys = json.loads(os.getenv("API_KEYS", "{}"))


class Crawler:
    """
    Crawler class for scraping university data.
    """
    
    def __init__(self, url: str = 'https://webapplis.utc.fr/uvs/index.xhtml', waiting_time: int = 2):
        """Initialize the Crawler object.
        
        Args:
            url (str, optional): The URL of the website to crawl. Defaults to 'https://webapplis.utc.fr/uvs/index.xhtml'.
            waiting_time (int, optional): Waiting time for page load after a request, in seconds. Defaults to 2.
        """
        
        self.url = url
        self.waiting_time = waiting_time
        self.chrome_options = Options()
        self.chrome_options.add_argument("--headless")
        self.chrome_options.add_argument("--no-sandbox")
        self.chrome_options.add_argument("--disable-dev-shm-usage")
        self.driver = webdriver.Chrome(chrome_options=self.chrome_options)
    
    # Private method to select a diploma from the web page
    def _select_diploma(self, diploma_code):
        """Selects a diploma from the dropdown.
        
        Args:
            diploma_code (str): The diploma code to select.
        """
        
        select_diploma = Select(self.driver.find_element(By.ID, "mainForm:sel_diplomes"))
        select_diploma.select_by_visible_text(diploma_code)
        time.sleep(self.waiting_time)
        
    # Private method to select a speciality in a diploma
    def _select_speciality(self, speciality):
        """Selects a speciality from the dropdown.
        
        Args:
            program (str): The program to select.
        """
        
        select_speciality = Select(self.driver.find_element(By.ID, "mainForm:brList"))
        select_speciality.select_by_visible_text(speciality)
        time.sleep(self.waiting_time)
    
    # Private method to extract content from the web page
    def _extract_content(self):
        """Extracts content from the web page.
        
        Returns:
            str: The extracted content.
        """
        
        content = ""
        for request in self.driver.requests:
            if request.url.startswith(self.url):
                content = decode(request.response.body, request.response.headers.get('Content-Encoding', 'identity')).decode("utf-8")
        return content
    
    # Private method to parse content using BeautifulSoup
    def _parse_content(self, content):
        """Parses content using BeautifulSoup.
        
        Args:
            content (str): The content to parse.
        
        Returns:
            BeautifulSoup: The parsed content.
        """
        
        return BeautifulSoup(BeautifulSoup(content, 'xml').text, 'html.parser')

    # Private method to extract specialitys using SeleniumWire and parse them using BeautifulSoup
    def _extract_and_parse_specialities(self, diplomas, diploma_code):
        """Extracts and parses specialities for the specified diploma.
        
        Args:
            diplomas (dict): The dictionary to store the extracted specialities.
            diploma_code (str): The diploma code.
        """
        
        # Capturing network requests and extracting content
        content = self._extract_content()
        soup = self._parse_content(content)
        
        # Extract specialities information
        sel_specialities = soup.find("select", {"id": "mainForm:brList"})
        if sel_specialities:
            specialities_to_add = []
            for option in sel_specialities.find_all('option'):
                existing_speciality = False
                if option.string != 'Toutes':
                    if diplomas[diploma_code] != {}:
                        for code, label in diplomas[diploma_code].items():
                            speciality = label + ' (' + code + ')'
                            if option.string == speciality:
                                existing_speciality = True
                    if not(existing_speciality):
                        speciality_code = re.findall(r'\((.*?)\)', option.string)[0]
                        speciality_label = option.string.replace(' (' + speciality_code + ')', "")
                        specialities_to_add.append((speciality_code, speciality_label))
            
            for speciality_code, speciality_label in specialities_to_add:
                diplomas[diploma_code][speciality_code] = speciality_label

    # Private method to extract uvs using SeleniumWire and parse them using BeautifulSoup
    def _extract_and_parse_uvs(self, uvs, diploma, speciality_code=None):
        """Extracts and parses UVs data.
        
        Args:
            uvs (dict): Dictionary to store the extracted UVs data.
            diploma (str): The diploma.
            program_code (str, optional): The program code. Defaults to None.
        """
        
        # Extract content
        content = self._extract_content()
        soup = self._parse_content(content)
        
        # Parse table containing courses information
        table = soup.find("table", {"class": "table table-striped"})
        if table:
            header = []
            for i, row in enumerate(table.find_all('tr')):
                if i == 0:
                    header = [el.text.strip() for el in row.find_all('th')]
                else:
                    course = [el.text.strip() for el in row.find_all('td')]
                    if course[0] not in uvs:
                        uvs[course[0]] = {}
                        for i in range(1, 3):
                            uvs[course[0]][header[i]] = course[i]
                        for i in range(5, len(course)):
                            if course[i] == 'Automne' or course[i] == 'Printemps':
                                uvs[course[0]]['Semestres'] = [course[i]]
                            elif course[i] == 'Automne, Printemps':
                                uvs[course[0]]['Semestres'] = ["Automne", "Printemps"]
                        uvs[course[0]]["Spécialités"] = []
            
                    if speciality_code:
                        uvs[course[0]]["Spécialités"].append(speciality_code)
                    else:
                        if diploma == "Bachelor Ingénierie Digitale et Management (ID&M)":
                            uvs[course[0]]["Spécialités"].append("ID-M")
                        elif diploma == "Licence professionnelle":
                            uvs[course[0]]["Spécialités"].append("MTSP")
                        elif diploma == "Master Erasmus Mundus EPOG":
                            uvs[course[0]]["Spécialités"] = ["Major A", "Major B", "Major C"]

    # Private method to clean up resources
    def _cleanup(self):
        """Cleans up resources.
        """
        
        del self.driver.requests

    # Public method to crawl diplomas and specialities from the web page
    def crawl_diplomas(self):
        """Crawls university UVs catalog.

        Returns:
            dict: Dictionnary with the Diplomas data recovered.
        """

        # Load the web page
        self.driver.get(self.url)
        
        # Initialize diplomas dictionary to store extracted information
        diplomas = {}
        
        # Extracting diplomas information
        source_page = self.driver.page_source
        soup = BeautifulSoup(source_page, 'html.parser')
        sel_diplomas = soup.find("select",  {"id":"mainForm:sel_diplomes"})
        if sel_diplomas:
            for option in sel_diplomas.find_all('option'):
                if option.string != "Sélectionnez une formation ..." and option.string not in diplomas:
                    diplomas[option.string] = {}
                    
        # Cleanup previous requests
        self._cleanup()
        
        # Iterate over diplomas
        for diploma_code in diplomas.keys():
            # Select diploma
            self._select_diploma(diploma_code)
            # Extract and parse diplomas
            self._extract_and_parse_specialities(diplomas, diploma_code)
                    
        # Sort diplomas and specialities alphabetically
        diplomas = {diploma_code: {k: v for k, v in sorted(specialities.items())} for diploma_code, specialities in sorted(diplomas.items())}
        
        return diplomas

    # Public method to crawl uvs from the web page
    def crawl_uvs(self):
        """Crawls university UVs catalog and return UV data in a dictionnary.
        
        Returns:
            dict: A dictionary containing the crawled UVs data.
        """
        
        # Load the web page
        self.driver.get(self.url)
        
        # Initialize uvs dictionary to store extracted information
        uvs = {}
        
        # Crawl diplomas first
        diplomas = self.crawl_diplomas()
            
        # Iterate over diplomas
        for diploma, specialities in diplomas.items():
            # Select the diploma
            select = Select(self.driver.find_element(By.ID, "mainForm:sel_diplomes"))
            select.select_by_visible_text(diploma)
            time.sleep(self.waiting_time)
            
            # Select "Toutes" from the specialities dropdown and extract UVs
            self._select_speciality("Toutes")
            # Extract UVs
            self._extract_and_parse_uvs(uvs, diploma)
            
            # Iterate over specialities for each diploma
            if diploma != "Bachelor Ingénierie Digitale et Management (ID&M)" or diploma != "Licence professionnelle" or diploma != "Master Erasmus Mundus EPOG":
                for speciality_code, speciality_label in specialities.items():
                    speciality = speciality_label + ' (' + speciality_code + ')'
                    # Select the speciality
                    self._select_speciality(speciality)
                    # Extract UVs
                    self._extract_and_parse_uvs(uvs, diploma, speciality_code)
                                
        # Sort UVs alphabetically
        uvs = {course_code: uvs[course_code] for course_code in sorted(uvs.keys())}
        
        return uvs
    

# Initialize FastAPI application
app = FastAPI()

# Initialize Redis client for caching
redis_client = redis.Redis(host='redis', port=6379)

# Initialize the Crawler
crawler = Crawler()


# Function to refresh cache
def refresh_cache():
    """Refreshes the cache with new data.
    """
    diplomas = crawler.crawl_diplomas()
    uvs = crawler.crawl_uvs()
    redis_client.set("diplomas", json.dumps(diplomas))
    redis_client.set("uvs", json.dumps(uvs))


# Schedule the cache refresh at 2 AM every day
scheduler = BackgroundScheduler()
scheduler.add_job(refresh_cache, 'cron', hour=2)
scheduler.start()


# Function to validate the provided API key
def check_api_key(api_key_header: str = Depends(api_key_header)):
    """Validate the provided API key.

    Args:
        api_key_header (str): The API key from the request header.
        settings (Settings): The Settings object containing valid API keys.

    Returns:
        str: The validated API key.

    Raises:
        HTTPException: If the API key is invalid.
    """
    
    if api_key_header in api_keys.values():
        return api_key_header
    else:
        raise HTTPException(status_code=403, detail="Invalid API Key")


@app.on_event("startup")
def startup_event():
    """FastAPI event handler for application startup.
    """
    refresh_cache()

@app.get("/", tags=["documentation"])
def get_documentation():
    """Return the documentation of the UTCrawl API.
    """
    documentation = """
    Welcome to the UTCrawl API Documentation!
    
    The UTCrawl API aims to retrieve data concerning UTC diplomas and UVs.
    The latter returns the data in the form of JSON dictionaries to the various endpoints.
    To use this API, please contact SiMDE at simde@assos.utc.fr to obtain an API key to access the service.

    Endpoints:
    1. diplomas:
        - Method: GET
        - Description: Returns the list of diplomas available in the university catalog.
        - Response: JSON containing the crawled diploma data.

    2. uvs:
        - Method: GET
        - Description: Returns the list of UVs (Unités de Valeur) available in the university catalog.
        - Response: JSON containing the crawled UV data.

    To access specific endpoints, use the following URLs:
    - diplomas: /diplomas
    - uvs: /uvs
    """
    return Response(content=documentation, media_type="text/plain; charset=utf-8")

@app.get("/diplomas", tags=["visualize-data"])
def get_diplomas(response: Response, api_key: str = Depends(check_api_key)):
    """Retrieve the list of diplomas.

    Args:
        response (Response): The FastAPI response object.
        api_key (str): The validated API key.

    Returns:
        dict: The cached diploma data.
    """
    
    data = redis_client.get("diplomas")
    if not data:
        diplomas = crawler.crawl_diplomas()
        redis_client.set("diplomas", json.dumps(diplomas))
        return diplomas
    return json.loads(data)

@app.get("/uvs", tags=["visualize-data"])
def get_uvs(response: Response, api_key: str = Depends(check_api_key)):
    """Retrieve the list of UVs.

    Args:
        response (Response): The FastAPI response object.
        api_key (str): The validated API key.

    Returns:
        dict: The cached UV data.
    """
    
    data = redis_client.get("uvs")
    if not data:
        uvs = crawler.crawl_uvs()
        redis_client.set("uvs", json.dumps(uvs))
        return uvs
    return json.loads(data)


if __name__ == "__main__":
    uvicorn.run("main:app", host="0.0.0.0", port=8001, reload=True)
